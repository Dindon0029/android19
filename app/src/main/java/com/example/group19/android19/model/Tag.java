package com.example.group19.android19.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;


/**
 * This class is to define the data type Tag
 * @author Zetao Yu
 * @author Ruoting Jia
 * */
public class Tag implements Serializable {

    /** Generate SerialVersionUID */
    private static final long serialVersionUID = 5956532667089012469L;

    /** type of the tag */
    private String type;

    /** value of the tag */
    private String value;

    /** the list of photos having this tag */
    ArrayList<Photo> photoList = null;

    /**
     * Constructor of Tag
     * @param type type of tag
     * @param value value of tag
     */
    public Tag(String type, String value) {
        this.type = type;
        this.value = value;
    }

    /**
     * get the type of the tag
     * @return type of the tag
     */
    public String getType() { return type; }

    /**
     * get the type of the tag
     * @return value of the tag
     */
    public String getValue() { return value; }

    /**
     * get the list of photos associating with this tag
     * @return list of photos
     */
    public ArrayList<Photo> getPhotoList() { return this.photoList; }

    /**
     * get a formated tag
     * @return a string of tag to display
     */
    public String toString() { return type + ": " + value; }

	/*
	 * Used for sorting photos by their tags
	 * @param t1 The first tag
	 * @param t2 The second tag
	 * @return compare value
	public class tagComparator implements  Comparator<Tag> {
		@Override
		public int compare(Tag t1, Tag t2) {
			int typeComp = t1.type.compareToIgnoreCase(t2.type);
			return typeComp == 0 ?  t1.value.compareToIgnoreCase(t2.value) : typeComp;
		}
	}

	private void sortByTag(ObservableList<Tag> tags) {
		tags.sort(new tagComparator());
	}
	*/
}
