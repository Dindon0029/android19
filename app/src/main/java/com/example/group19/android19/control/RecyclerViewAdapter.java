package com.example.group19.android19.control;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.group19.android19.R;
import com.example.group19.android19.model.Photo;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>{
    @NonNull

    private Context mContext;
    private List<Photo> mData;

    public RecyclerViewAdapter(Context mContext, List<Photo> mData){
        this.mContext = mContext;
        this.mData = mData;
    }
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_photo, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        //NEED TO FIX setImageResource!!!
        holder.photo_title.setText(mData.get(position).getCaption());
        holder.photo_thumbnail.setImageResource(R.drawable.eiffel_tower);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView photo_title;
        ImageView photo_thumbnail;

        public MyViewHolder(View itemView){
            super(itemView);

            photo_title = (TextView) itemView.findViewById(R.id.photo_name_id);
            photo_thumbnail = (ImageView) itemView.findViewById((R.id.photo_image_id));

        }
    }
}
