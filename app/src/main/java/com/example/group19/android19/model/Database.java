package com.example.group19.android19.model;

import java.io.*;
import java.util.ArrayList;

/**
 * This class is to define the data type Database, which is used to serialize and save the users info
 * @author Zetao Yu
 * @author Ruoting Jia
 * */

public class Database implements Serializable{

    /** a list of albums */
    public static ArrayList<Album> albumList = new ArrayList<Album>();

    /** directory of the serialized data */
    public static final String storeDir = "/";

    /** file of the serialized data */
    public static final String storeFile = "users.txt";

    /**
     * Constructor of Database
     */
    public Database() {}

    public static ArrayList<String> getNameList(){

        ArrayList<String> namelist = new ArrayList<String>();
        if (Database.albumList.size() < 1) {
            return namelist;
        }

        for (Album i : albumList){
            namelist.add(i.getAlbumName());
        }

        return namelist;
    }

    /**
     * write a new database to file
     * @param db database to be written to the file
     * @throws IOException
     */
    public static void writeApp(Database db) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("save"));
        oos.writeObject(db);
    }
    /**
     * read from the existing database to the app
     * @return a new Database object
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Database readApp() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("save"));
        Database db = (Database)ois.readObject();
        return db;
    }
}
