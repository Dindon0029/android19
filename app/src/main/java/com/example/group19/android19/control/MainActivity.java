package com.example.group19.android19.control;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.content.SharedPreferences;
import android.content.Context;
import android.view.View.OnClickListener;

import com.example.group19.android19.R;
import com.example.group19.android19.model.Album;
import com.example.group19.android19.model.Database;

import org.w3c.dom.Text;

import java.util.ArrayList;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private AlertDialog.Builder builder;
    private String newname;

    private static Database database = new Database();
    private Album currentAlbum = null;
    private ArrayList<String> AlbumNameList = new ArrayList<String>();
    static String currentAlbumName = "";
    private int currentAlbumIndex;

    private String album_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get the button reference from XML layout
        Button add = (Button) findViewById(R.id.add);
        add.setOnClickListener(this);

        Button delete = (Button) findViewById(R.id.delete);
        delete.setOnClickListener(this);

        Button rename = (Button) findViewById(R.id.rename);
        rename.setOnClickListener(this);

        Button open = (Button) findViewById(R.id.open);
        open.setOnClickListener(this);

        /*
        //Read the user information from local
        ArrayList<String> alname= new ArrayList<String>();
        ArrayList<String> phname= new ArrayList<String>();
        ArrayList<String> path= new ArrayList<String>();
        */
        initialize();
    }

    public void initialize(){
        try {
            database = Database.readApp();
        } catch (ClassNotFoundException e1) { e1.printStackTrace();
        } catch (IOException e1) { e1.printStackTrace(); }
        refreshInfo();
    }

    public void refreshInfo(){

        final ListView mainList = (ListView) findViewById(R.id.main_listview);

        AlbumNameList = database.getNameList();

        ListAdapter listad = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, AlbumNameList);

        mainList.setAdapter(listad);

        mainList.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        album_name = String.valueOf(adapterView.getItemAtPosition(i));

                        currentAlbumName = album_name;
                        currentAlbum = Tools.findAlbum(database.albumList, currentAlbumName);
                        currentAlbumIndex = database.albumList.indexOf(currentAlbum);

                        Toast.makeText(MainActivity.this, "You selected: " + album_name,Toast.LENGTH_SHORT).show();
                    }
                }
        );
    };

    //Handle four buttons' operation
    @Override
    public void onClick(View v) {
        // default method for handling onClick Events..
        switch (v.getId()) {

            case R.id.add:
                addAlbum();
                break;

            case R.id.delete:
                deleteAlbum();
                break;

            case R.id.rename:
                renameAlbum();
                break;

            case R.id.open:
                openAlbum();
                break;

            default:
                break;
        }
    }

    public void openAlbum(){
        if(currentAlbumName.length() < 1){
            Toast.makeText(MainActivity.this, "Please select an album",Toast.LENGTH_SHORT).show();
            return;
        }
        Intent photo_window = new Intent(MainActivity.this, PhotoActivity.class);
        startActivity(photo_window);
    }

    public void deleteAlbum(){

        // no name input
        if(currentAlbumName == null)
            return;

        // else create a new album and update data
        database.albumList.remove(currentAlbum);
        Toast.makeText(MainActivity.this, "Deleted " + currentAlbumName,Toast.LENGTH_SHORT).show();

        try{
            Database.writeApp(database);
            database = Database.readApp();
        }catch(IOException e1) { e1.printStackTrace(); }
        catch (ClassNotFoundException e) { e.printStackTrace(); }

        refreshInfo();
    }

    public void renameAlbum(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);

        View mView = getLayoutInflater().inflate(R.layout.dialogue, null);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        Button addConfirm = (Button) mView.findViewById(R.id.addConfirm);
        Button cancel = (Button) mView.findViewById(R.id.cancel);

        final EditText text = (EditText) mView.findViewById(R.id.newname);

        addConfirm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                newname = text.getText().toString();
                if(newname.length() < 1){
                    Toast.makeText(MainActivity.this, "Album name cannot be empty",Toast.LENGTH_SHORT).show();
                }
                else{
                    // renaming
                    if(newname == null)
                        return;

                    // else rename the album and update data
                    currentAlbum.renameAlbum(newname);
                    database.albumList.remove(currentAlbum);
                    database.albumList.add(currentAlbum);

                    try{
                        Database.writeApp(database);
                    }catch(IOException e1) { e1.printStackTrace(); }

                    dialog.cancel();
                    refreshInfo();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                dialog.cancel();
                Toast.makeText(MainActivity.this, "canceled",Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Pop up a dialog for name or rename

    public void addAlbum() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);

        View mView = getLayoutInflater().inflate(R.layout.dialogue, null);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        Button addConfirm = (Button) mView.findViewById(R.id.addConfirm);
        Button cancel = (Button) mView.findViewById(R.id.cancel);

        final EditText text = (EditText) mView.findViewById(R.id.newname);

        addConfirm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                newname = text.getText().toString();
                if(newname.length() < 1){
                    Toast.makeText(MainActivity.this, "Album name cannot be empty",Toast.LENGTH_SHORT).show();
                }
                else{
                    //adding
                    if(newname == null)
                        return;

                    // else create a new album and update data
                    Album newalbum = new Album(newname);
                    database.albumList.add(newalbum);

                    try{
                        Database.writeApp(database);
                        database = Database.readApp();
                    }catch(IOException e1) { e1.printStackTrace(); }
                    catch (ClassNotFoundException e) { e.printStackTrace(); }

                    dialog.cancel();
                    Toast.makeText(MainActivity.this, "added an album: " + newname,Toast.LENGTH_SHORT).show();
                    refreshInfo();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                dialog.cancel();
                Toast.makeText(MainActivity.this, "canceled",Toast.LENGTH_SHORT).show();
            }
        });

    }
}