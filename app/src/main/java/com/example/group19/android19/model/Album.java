package com.example.group19.android19.model;


import java.io.Serializable;
import java.util.ArrayList;


/**
 * This class is to define the data type Album
 * @author Zetao Yu
 * @author Ruoting Jia
 * */
public class Album implements Serializable {

    /** Generate SerialVersionUID */
    private static final long serialVersionUID = -340491349554805923L;

    /** name of this album */
    private String album_name;

    /** number of photo in this album */
    private int num_photo;

    /** earliest date of all the photos */
    private String start_date;

    /** latest date of all the photos */
    private String end_date;

    /** a list of photos in this album */
    public ArrayList<Photo> photoList = new ArrayList<Photo>();

    /**
     * Constructor of Album
     * @param album_name name of the album
     */
    public Album(String album_name) {
        this.album_name = album_name;
    }

    /**
     * get album name
     * @return this album name
     */
    public String getAlbumName() { return this.album_name; }

    /**
     * get start time
     * @return this album start time
     */
    public String getStartTime() { return start_date; }

    /**
     * get end time
     * @return this album end time
     */
    public String getEndTime() { return end_date; }

    /**
     * get a string of date range
     * @return a string of date range
     */
    public String getDateTime() { return start_date + " - " + end_date; }

    /**
     * get number of photos
     * @return number of photos in String format
     */
    public String getPhotoNum() { return Integer.toString(num_photo);}

    /**
     * get number of photos
     * @return number of photos in integer format
     */
    public int getIntPhoto() { return this.num_photo; }

    /**
     * get an arrayList of photos
     * @return an arrayList of photos
     */
    public ArrayList<Photo> getPhotoList() { return this.photoList; }

    /**
     * rename an existing album
     * @param newName the new name to replace
     */
    public void renameAlbum(String newName) { this.album_name = newName; }

    /**
     * add photo to the photoList, and increment 1 to number of photos in the album
     * @param p new photo
     */
    public void createPhoto(Photo p) {
        this.num_photo++;
        photoList.add(p);
        updateRange();
    }

    /**
     * update total number of photos
     */
    public void updateNum() {
        this.num_photo = photoList.size();
    }

    /**
     * update the date range of this album
     */
    public void updateRange() {
        //first photo
        if (photoList.size() == 0) {
            start_date = null;
            end_date = null;
            return;
        }

        //first photo
        if (photoList.size() == 1) {
            start_date = photoList.get(0).getDateTime();
            end_date = photoList.get(0).getDateTime();
            return;
        }

        //
        if (start_date == null || end_date == null) {
            start_date = "2018/04/11 19:00";
            end_date = "2018/04/11 19:00";
        }

        for (Photo p: photoList) {
            if (p.getDateTime().compareTo(start_date) < 0) {
                start_date = p.getDateTime();
                break;
            }

            if (p.getDateTime().compareTo(end_date) > 0) {
                end_date = p.getDateTime();
                break;
            }
        }

    }

    /**
     * delete a photo from the album
     * @param p the photo to be deleted
     */
    public void deletePhoto(Photo p) {
        photoList.remove(p);
    }
}
