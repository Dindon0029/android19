package com.example.group19.android19.control;

import com.example.group19.android19.model.Album;

import java.util.ArrayList;

public class Tools {
    public static Album findAlbum(ArrayList<Album> albumList, String name){
        for (Album i : albumList) {
            if (i.getAlbumName().equals(name)) return i;
        }
        return null;
    }
}
