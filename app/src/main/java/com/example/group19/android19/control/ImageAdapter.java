package com.example.group19.android19.control;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.group19.android19.R;

public class ImageAdapter extends BaseAdapter {
    private Context context;
    public Integer[] images = {
            R.drawable.colosseum,
            R.drawable.eiffel_tower,
            R.drawable.grand_canyon,
            R.drawable.gulangyu,
            R.drawable.oriental_pearl_tower,
            R.drawable.phoenix_island,
            R.drawable.tiananmen,
            R.drawable.time_square,
            R.drawable.white_house
    };

    public ImageAdapter(Context c){
        context = c;
    }

    @Override
    public int getCount(){
        return images.length;
    }

    @Override
    public Object getItem(int position){
        return images[position];
    }

    @Override
    public long getItemId(int position){
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ImageView image = new ImageView(context);
        image.setImageResource(images[position]);
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        image.setLayoutParams(new GridView.LayoutParams(240, 240));
        return image;
    }
}
