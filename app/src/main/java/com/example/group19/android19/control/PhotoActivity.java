package com.example.group19.android19.control;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.example.group19.android19.R;
import com.example.group19.android19.model.Photo;

import java.util.ArrayList;
import java.util.List;

public class PhotoActivity extends AppCompatActivity implements View.OnClickListener {

    List<Photo> photoList;
    static String caption = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        Toolbar album_title = (Toolbar) findViewById(R.id.album_title);
        album_title.setTitle(MainActivity.currentAlbumName);

        Button add = (Button) findViewById(R.id.add_photo);
        add.setOnClickListener(this);
        Button remove = (Button) findViewById(R.id.remove_photo);
        remove.setOnClickListener(this);
        Button display = (Button) findViewById(R.id.display_photo);
        display.setOnClickListener(this);

        GridView gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(new ImageAdapter(this));

        /*
        photoList = new ArrayList<>();
        photoList.add(new Photo("hey", "date", "path", null));

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_id);

        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, photoList);

        myrv.setLayoutManager(new GridLayoutManager(this, 3));
        */
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_photo:
                // insert function
                break;

            case R.id.remove_photo:
                // insert function
                break;

            case R.id.display_photo:
                // insert function
                break;

            default:
                break;
        }
    }

    public void displayPhoto(){
        if(caption.length() < 1){
            Toast.makeText(PhotoActivity.this, "Please select a photo",Toast.LENGTH_SHORT).show();
            return;
        }
        // NULL SHOULD BE CHANGED!!!!!!!!!!
        Intent photo_window = new Intent(PhotoActivity.this, null);
        startActivity(photo_window);
    }
}
