package com.example.group19.android19.model;

import android.media.Image;
import android.nfc.Tag;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * This class is to define the data type Photo
 * @author Zetao Yu
 * @author Ruoting Jia
 * */
public class Photo implements Serializable{

    /** Generate SerialVersionUID */
    private static final long serialVersionUID = -4191487072423720041L;

    /** the image of this photo */
    transient private Image image;

    /** caption of this photo */
    private String caption;

    /** date of the photo */
    private String dateTime;

    /** path of this photo */
    public String filepath;

    /** album of this photo */
    private Album current_album;

    /** store the list of albums */
    public ArrayList<Album> albumList = new ArrayList<Album>();

    /** list of tags as Tag objects */
    public ArrayList<Tag> tagList = new ArrayList<Tag>();

    /** list of tags as String objects */
    public ArrayList<String> tagListinString = new ArrayList<String>();

    /**
     * Constructor of photo
     * @param caption caption of this photo
     * @param date imported date of this photo
     * @param filepath file path of this photo
     * @param album album of this photo
     */
    public Photo(String caption, String date, String filepath, Album album) {
        this.caption = caption;
        this.filepath = filepath;
        this.dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm"));
        //this.image = new Image(filepath);
        this.current_album = album;
        this.albumList.add(current_album);
    }

    /**
     * get the caption of this photo
     * @return caption of this photo
     */
    public String getCaption() { return this.caption; }

    /**
     * get the date and time of this photo
     * @return date and time of this photo
     */
    public String getDateTime() { return this.dateTime; }

    /**
     * update the image to this photo
     * @return image format of this photo

    public Image getImage() {
        if(this.filepath == null) {return null;}
        Url2Image(this.filepath);
        return this.image;
    }
    */
    /*
    /**
     * update the taglist of the string format

    @SuppressWarnings("unchecked")
    public void getTagListInString() {
        this.tagListinString = (ArrayList<String>) this.tagList.stream().map(t -> t.toString());
    }
    */

    /**
     * convert the URL to image
     * @param url URL of photo

    public void Url2Image(String url) {
        //System.out.println("Current URL is "+url);
        this.image = new Image(new File(url).toURI().toString());
    }
    */

    /**
     * re-caption this photo
     * @param newCaption caption to replace
     */
    public void recaptionPhoto(String newCaption) {
        this.caption = newCaption;
    }

    /**
     * add a new tag to the photo
     * @param type type of the tag
     * @param value value of the tag
     */
    public void addTag(String type, String value) {
        //Tag tag = new Tag(type, value);
        //tagList.add(tag);
    }

    /**
     * delete a tag from the photo
     * @param type type of the tag
     * @param value value of the tag

    public void deleteTag(String type, String value) {
        for (Tag t: tagList) {
            if (t.getType().equals(type) && t.getValue().equals(value)) {
                tagList.remove(t);
                break;
            }
        }
    }
    */

    /**
     * Copy current photo to another album
     * @param copyToAlbum album where the photo to be copied to
     */
    public void copyToAnotherAlbum(Album copyToAlbum) {
        if (!albumList.contains(copyToAlbum)) {
            albumList.add(copyToAlbum);
        }else {
            // handle ERROR message
        }
    }

    /**
     * Move current photo to another album
     * @param moveToAlbum album where the photo to be moved to
     */
    public void moveToAnotherAlbum(Album moveToAlbum) {
        if (!albumList.contains(moveToAlbum)) {
            albumList.add(moveToAlbum);
        }else {
            // handle ERROR message
        }
        current_album = null;
    }
    public Image getImage(){
        return this.image;
    }
}

